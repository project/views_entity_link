<?php

namespace Drupal\views_entity_link\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\views\Plugin\views\field\EntityLink as EntityLinkBase;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to an entity using entity link templates.
 *
 * @ViewsField("views_entity_link")
 */
class EntityLink extends EntityLinkBase {

  use RedirectDestinationTrait;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['link_template'] = ['default' => ''];
    $options['use_ajax'] = ['default' => FALSE];
    $options['destination'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $def = $this->entityTypeManager->getDefinition($this->definition['entity_type']);
    $templates = array_keys($def->getLinkTemplates());
    $templates = array_combine($templates, $templates);

    $form['link_template'] = [
      '#type' => 'select',
      '#title' => $this->t('Link template'),
      '#default_value' => $this->options['link_template'],
      '#options' => $templates,
      '#required' => TRUE,
    ];

    $form['use_ajax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use AJAX'),
      '#default_value' => $this->options['use_ajax'],
    ];

    $form['destination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include destination query parameter'),
      '#default_value' => $this->options['destination'],
    ];

    parent::buildOptionsForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityLinkTemplate() {
    return $this->options['link_template'];
  }

  /**
   * {@inheritdoc}
   */
  public function renderAsLink($alter, $text, $tokens) {
    $text = $this->viewsTokenReplace($text, $tokens);

    $render = [
      '#type' => 'link',
      '#title' => $text,
      '#url' => $alter['url'],
    ];

    if (!empty($this->options['use_ajax'])) {
      $render['#options']['attributes']['class'][] = 'use-ajax';
      $render['#attached']['library'][] = 'core/drupal.ajax';
      $render['#url']->setRouteParameter('js', 'nojs');
    }

    $this->getModuleHandler()->alter('views_entity_link', $render, $this->view, $this->options);
    $this->getThemeManager()->alter('views_entity_link', $render, $this->view, $this->options);

    if (!empty($alter['prefix'])) {
      $render['#prefix'] = $this->viewsTokenReplace($alter['prefix'], $tokens);
    }
    if (!empty($alter['suffix'])) {
      $render['#suffix'] = $this->viewsTokenReplace($alter['suffix'], $tokens);
    }

    return $this->getRenderer()->render($render);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->field_alias == 'unknown') {
      $this->field_alias = $this->realField;
    }
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    $url = parent::getUrlInfo($row);

    if ($this->options['destination']) {
      $params = $url->getRouteParameters();
      $params['destination'] = $this->getRedirectDestination()->get();
      $url->setRouteParameters($params);
    }

    return $url;
  }

  /**
   * Gets the theme service.
   *
   * @return \Drupal\Core\Theme\ThemeManagerInterface
   *   The theme service.
   */
  protected function getThemeManager() {
    if (!$this->themeManager) {
      $this->themeManager = \Drupal::theme();
    }

    return $this->themeManager;
  }

}
