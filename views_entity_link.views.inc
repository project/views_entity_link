<?php

/**
 * @file
 * Views Entity Link views integration.
 */

use Drupal\Core\Entity\ContentEntityType;

/**
 * Implements hook_views_data_alter().
 */
function views_entity_link_views_data_alter(&$data) {
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $definition) {
    if (!$definition instanceof ContentEntityType) {
      continue;
    }

    $data[$definition->getBaseTable()]['views_entity_link'] = [
      'title' => t('Entity link'),
      'help' => t('Provide a link using the entity type link templates.'),
      'field' => [
        'id' => 'views_entity_link',
      ],
      'real field' => $definition->getKey('id'),
      'entity_type' => $definition->id(),
    ];
  }

}
