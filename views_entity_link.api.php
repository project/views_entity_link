<?php

/**
 * @file
 * Documentation for views_entity_link API.
 */

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;

/**
 * Alter views entity links.
 *
 * This may be utilized by modules or themes.
 *
 * @param array &$build
 *   Render array for the link.
 * @param \Drupal\views\ViewExecutable $view
 *   The view.
 * @param array $options
 *   The views field options.
 */
function hook_views_entity_link_alter(array &$build, ViewExecutable $view, array $options) {
  // Add a class for my link.
  if ($options['link_template'] == 'my-entity-link') {
    $build['#options']['attributes']['class'][] = Html::getClass('my-link');
  }
}
